<div id="bloques" class="bloquedebajo">
	<div id="redondear" style="background-color: rgb(126, 124, 124);height: 66px;"><p class="titulos">Acerca De</p></div>
	
	<p class="textoprincipal">
		Somos una empresa que se está consolidando en el mercado de transporte de carga en mexico. Nuestros años de experiencia son nuestro respaldo y la calidad de servicio nuestra mejor carta de presentación.

		Nos estamos preparando día a día para atender las diversas necesidades de carga de nuestros clientes, por ello tenemos un servicio personalizado, el cual es completo y comprende: la carga, descarga y entrega de la mercadería en el almacén del cliente o donde nos indiquen. 

		Nuestro compromiso es ofrecerle un servicio de primer nivel, con garantía, seguridad y confianza, cumpliendo los tiempos de entrega.
	</p>

</div>
<div id="shade"></div>