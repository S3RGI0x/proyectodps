<?php
  $page = basename($_SERVER['SCRIPT_FILENAME']);
  $page = str_replace('_',' ',$page);
  $page = str_replace('.php','',$page);
  $page = ucfirst($page);
?>
<div id="imagenes">
 <img src="imagenes/img1.png" class="imagen1" alt="">
 <img src="imagenes/img2.png" class="imagen2" alt=""> 
</div>

<div id="texto">
  <p> ¿Quieres conocer mas Acerca de Nosotros?</p>
  <br/>
  <center><button class="clean-gray" onclick="window.location.href='?pagina=acerca_de'">VER</button></center>
</div>

<div id="tablas">                   
  <div class="cols-3">
    <div class="pricing_box radius-right">
      <div class="header">
        <span>Paso 1</span>
      </div>
      <h3>Inicia Sesion o Registrate</h3>
      <ul>
        <li class="even"><strong class="yes">Inicia Sesion o Registrate  para que llenes y nos envies tu solicitud o dudas al respecto de nuestros servicios.</strong></li>
        <!--<li class="odd"><strong class="yes">Group (3 or more) - 20% discount</strong></li> -->
      </ul>

      <p class="button signup"><a onClick="document.getElementById('open').click();">Iniciar Session / Registro</a></p>
    </div>

    <div class="pricing_box large radius-left">
      <div class="header">
        <span>Paso 2</span>
      </div>
      <h3>Llena tu Solicitud</h3>
      <ul>
        <li class="even"><strong class="yes">Llena todos los campos correctamente y completa el formulario. <p><p></strong></li>

        <!-- <li class="odd"><strong class="yes">Group (3 or more) - 20% discount</strong></li> -->
      </ul>



      <p class="button signup"><a href="http://itzurkarthi.com/contact">Realizar Solicitud</a></p>
    </div>

    <div class="pricing_box radius-right">
      <div class="header">
        <span>Paso 3</span>
      </div>
      <h3>Espera tu Respuesta</h3>
      <ul>
        <li class="even"><strong class="yes">Espera tu Respuesta. Nos comunicaremos contigo lo antes posible via Email, Telefono o puedes verificar tus Solicitudes aqui mismo</strong></li>

        <!--<li class="odd"><strong class="yes">Nos comunicaremos contigo.</strong></li>-->
      </ul>


      <p class="button signup"><a href="http://itzurkarthi.com/contact">Solicitudes Pendientes</a></p>
    </div>
  </div>

</div>