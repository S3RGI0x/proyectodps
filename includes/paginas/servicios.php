<div id="bloques" class="bloquedebajo">
	<div id="redondear" style="background-color: rgb(126, 124, 124);height: 66px;"><p class="titulos">Servicios</p></div>
	
	<p class="textoprincipal">
		Infraestructura
		Contamos con unidades propias, con capacidades de carga de 28 y 30 toneladas, las que se encuentran debidamente acondicionadas para realizar el tipo de traslado que soliciten y con todos los requisitos exigidos por el estandar de normatividad.
		Todas nuestras unidades cuentan con equipos GPS.
		Las unidades se encuentran debidamente intercomunicadas con nuestra base a través de la red NEXTEL para el trabajo Lima, y de la red RPM para el trabajo en provincia. La comunicación es constante, por lo que nuestros clientes estarán informados permanentemente de la ubicación y el estado de su mercadería.
		Brindamos servicio de resguardo y/o custodía para cargas especiales.
	</p>

</div>
<div id="shade"></div>