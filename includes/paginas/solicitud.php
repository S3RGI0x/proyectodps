<div id="bloques" class="bloquedebajo">
  <div id="redondear" style="background-color: rgb(83, 76, 76);;height: 66px;"><p class="titulos">Solicitud</p></div>
  

  <script type="text/javascript">

  var map;
  var gdir;
  var geocoder = null;
  var addressMarker;

  function initialize() {
    if (GBrowserIsCompatible()) { 
      map = new GMap2(document.getElementById("mapa_ruta"));
      map.addControl(new GLargeMapControl());
      map.addControl(new GMapTypeControl());
      gdir = new GDirections(map, document.getElementById("direcciones"));
      GEvent.addListener(gdir, "load", cargarRutas);
      GEvent.addListener(gdir, "error", handleErrors);

      setDirections("Leon de los Aldama", "Guanajuato", "mx"); 
      
    }
  } 

  function setDirections(fromAddress, toAddress, locale) {
    gdir.load("from: " + fromAddress + " to: " + toAddress,
      { "locale": locale });
    //cargarRutas();
  }

  function handleErrors(){
    if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
      alert("Dirección no disponible.\nError code: " + gdir.getStatus().code);
    else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
      alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code); 
    else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
      alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code); 
    else if (gdir.getStatus().code == G_GEO_BAD_KEY)
      alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);
    else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
      alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code); 
    else alert("An unknown error occurred."); 
  }

  function onGDirectionsLoad(){ 

  }

  </script>

<p>
  <div id="contacto">
    <form class="form" action="#" onsubmit="setDirections(this.from.value, this.to.value, this.locale.value); return false" name="form" style="margin-left: 22px; margin-top: 20px;">

    <p class="origen">
      <label for="name">Origen &nbsp;</label>
      <input type="text" name="from" id="fromAddress" style="width: 603px;"/>
    </p>

    <p class="Destino">
      <label for="email">Destino</label>
      <input type="text" name="to" id="toAddress" style="width: 603px;" />   
    </p>
    <input name="locale" type="hidden" id="locale" value="mx" size="25"/><br>

    <p class="submit" style="position: relative;float: right;margin-top: -113px;margin-right: 32px;">
        <input type="submit" name="Submit" value="Calcular Ruta" style="height: 81px; width: 222px;"/>
    </p>

  </form> 
</div>

  <div id="mapa_ruta"></div>
  <div id="shade"></div>


  <div id="direcciones" style="width: 710px; display:none;"></div> 

 <div id = "mostrarDatos" style="display:none;">
  <div id="sorigen1"><p id="sorigen"><p></div>
  <div id="sdestino1"><p id="sdestino"><p></div>
  <div id="skm1"><p id="skm"><p></div>
 </div>

<center><button class="clean-gray" onclick="window.location.href='?pagina=acerca_de'">Enviar Solicitud</button></center>

</div>
<div id="shade"></div>

