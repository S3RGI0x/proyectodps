<?php
session_start();
$html = '';
if(isset($_SESSION["idUsuario"])){

	if($_SESSION["tipoUsuario"] == 'cliente'){

		$html .= '<li class="left">&nbsp;</li>
		<li><font color="#ff0000">Hola</font> '.$_SESSION["nombres"]." ".$_SESSION["apellidos"].'</li>
		<li class="sep">|</li>
		<li><a href="?pagina=Solicitud">Realizar Solicitud</a></li>
		<li class="sep">|</li>
		<li><a href="#">Notificaciones(0)</a></li>
		<li class="sep">|</li>
		<li><a href="#">Modificar Datos Personales</a></li>
		<li class="sep">|</li>
		<li id="toggle">
		<a href="salir.php">Salir</a>	
		</li>
		<li class="right">&nbsp;</li>';
	}else{

		$html .= '<li class="left">&nbsp;</li>
		<li><font color="#ff0000">Hola</font> '.$_SESSION["nombres"]." ".$_SESSION["apellidos"].'</li>
		<li class="sep">|</li>
		<li><a href="#">Panel de Administracion</a></li>
		<li class="sep">|</li>
		<li id="toggle">
		<a href="salir.php">Salir</a>	
		</li>
		<li class="right">&nbsp;</li>';

	}

}else{

	$html .= '<li class="left">&nbsp;</li>
	<li><font color="#ff0000">Hola</font> Invitado</li>
	<li class="sep">|</li>
	<li id="toggle">
	<a id="open" class="open" href="#">Login | Registro</a>
	<a id="close" style="display: none;" class="close" href="#">Minimizar Panel</a>			
	</li>
	<li class="right">&nbsp;</li>';
}


?>
<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="content clearfix">
			
			<div class="left right">			
				<!-- Register Form -->
				<form id="FormularioRegistro" action="funciones/registro.php" method="POST">
					<h1>¿Aun no estas Registrado? Registrate!</h1>				
					<label class="grey" for="nombre">Nombre(s):</label>
					<input type="text" name="nombres" id="nombres" value="" size="23" class="required" minlength="5"/>
					<label class="grey" for="apellidos">Apellidos:</label>
					<input  type="text" name="apellidos" id="apellidos" size="23" class="required" minlength="4" />
					<label class="grey" for="email">Email:</label>
					<input  type="text" name="email" id="email" size="23" class="required email" />
					<label class="grey" for="telefono">Telefono:</label>
					<input type="text" name="telefono" id="telefono" size="23" class="required number" minlength="10" />
					<label class="grey" for="Estado">Estado:</label>
					<input  type="text" name="estado" id="estado" size="23" class="required" minlength="5"/>
					<label class="grey" for="municipio">Munucipio:</label>
					<input  type="text" name="municipio" id="municipio" size="23" class="required" minlength="4" />
					<label class="grey" for="direccion">Direccion:</label>
					<input  type="text" name="direccion" id="direccion" size="23" class="required" minlength="10"/>
					<label class="grey" for="cp">Codigo Postal:</label>
					<input  type="text" name="cp" id="cp" size="23" class="required number" minlength="5"/>
					<label class="grey" for="cp">RFC:</label>
					<input  type="text" name="RFC" id="RFC" size="23" class="required number" minlength="5"/>

					<center><p>[Elige Un usuario y contraseña]</p></center>
					<label class="grey" for="usuario">Usuario:</label>
					<input type="text" name="usuario" id="cpusuario" size="23" class="required" minlength="5"/>
					<label class="grey" for="contra">Contraseña:</label>
					<input  type="password" name="contra" id="contra" size="23" class="required" minlength="6" />
					<label class="grey" for="contra2">Repita Contraseña:</label>
					<input  type="password" name="contra2" id="contra2" size="23" class="required" minlength="6" />
					<br/>
					<input type="submit" name="submit" value="Registrar" class="bt_register" />
				</form>
				<div id="resp2"></div>
			</div>

			<div class="left right" style="margin-top: 101px; margin-left: 85px; width: 417px;border-left: 1px solid #333;">

				<!-- Login Form -->
				<form id="FormularioLogin" action="funciones/login.php" method="POST">
					<h1>Login de Usuario</h1>
					<label class="grey" for="log">Usuario:</label>
					<input  type="text" name="usuario" id="usuario" value="" size="23" class="required"/>
					<label class="grey" for="pwd">Contraseña:</label>
					<input type="password" name="contra" id="contra" size="23" class="required"/>
	            
        			<div class="clear"></div>
					<input type="submit" name="HacerLogin" id="HacerLogin" value="Login" class="bt_login" />
					<!--<button id="HacerLogin" class="clean-gray">Login</button>-->
					<a class="lost-pwd" href="?pagina=Recuperar_Contrasenia">¿Perdiste tu Contraseña?</a>
				</form>
				<div id="resp"></div>
			</div>

		</div>
</div> <!-- /login -->	

	<!-- The tab on top -->	
	<div class="tab">
		<ul class="login">
			<?php echo $html; ?>
		</ul> 
	</div> <!-- / top -->
	<!--panel -->
</div>