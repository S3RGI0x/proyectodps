<?php
//session_start();
require_once("requieres/funciones.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>ProyectoDPS | <?php if (!isset($_GET['pagina'])){echo 'Inicio';}else{ echo $_GET['pagina']; } ?></title>


  <!-- Librerias y Estilos (Principal) -->
  <link href="css/principal.css" rel="stylesheet" type="text/css" />
  <link href="css/styles.css" rel="stylesheet" type="text/css" />
  <!-- Menu Top -->
  <script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script src="js/slide.js" type="text/javascript"></script>
  <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />	
  <!-- Validaciones -->
  <script src="js/jquery-latest.js" type="text/javascript"></script>
  <script src="js/jquery.validate.js" type="text/javascript"></script>
  <script src="js/funciones.js" type="text/javascript"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
  
  <!-- API Google Maps -->
  <script src="http://maps.google.com/maps?file=api&amp;v=2&oe=ISO-8859-1;&amp;key=ABQIAAAAXfyLOV-DBHsmkpuY-LUUzBRvMuQNe3bCQ9tCDXjXwHZUjgdNBhQG32AJg5mKqo03Qmq9WX7GTGdmvw" type="text/javascript"></script> 


</head>
<body onload="initialize()" onunload="GUnload()">
  <script type="text/javascript">

var map;
var gdir;
var geocoder = null;
var addressMarker;

function initialize() {
if (GBrowserIsCompatible()) { 
map = new GMap2(document.getElementById("mapa_ruta"));
map.addControl(new GLargeMapControl());
map.addControl(new GMapTypeControl());
gdir = new GDirections(map, document.getElementById("direcciones"));
GEvent.addListener(gdir, "load", onGDirectionsLoad);
GEvent.addListener(gdir, "error", handleErrors);

setDirections("Leon de los Aldama", "Guanajuato", "mx"); 

}
} 

function setDirections(fromAddress, toAddress, locale) {
gdir.load("from: " + fromAddress + " to: " + toAddress,
{ "locale": locale });
}

function handleErrors(){
if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
alert("Dirección no disponible.\nError code: " + gdir.getStatus().code);
else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code); 
else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code); 
else if (gdir.getStatus().code == G_GEO_BAD_KEY)
alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);
else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code); 
else alert("An unknown error occurred."); 
}

function onGDirectionsLoad(){ 
}

</script>

  <!-- Menu Login Registro -->
  <?php include('includes/menuTop.php'); ?>

  <!-- imagen de fondo -->
  <img src="imagenes/globe.png" id="fondo" alt="">

  <!-- Menu Principal -->
  <?php include('includes/menu.php'); ?>

  <!-- contenido principal -->
  <div id="contenido">


    <div id="bloques" class="bloquedebajo">
      <p class="titulos">Solicitud </p>

      <form action="#" onsubmit="setDirections(this.from.value, this.to.value, this.locale.value); return false" name="form">
        Origen: <input type="text" size="25" id="fromAddress" name="from"/>
        Destino: <input name="to" type="text" id="toAddress" size="25"/><br>


        Idioma: <select id="locale" name="locale">
        <option value="es" selected>Castellano</option>
        <option value="en">English</option>
        <option value="fr">French</option>
        <option value="de">German</option>
        <option value="ja">Japanese</option>
      </select>

      <input type="submit" name="Submit" value="Calcular Ruta"/>
    </form> 

    <div id="mapa_ruta" style="width: 710px; height: 300px; border: 4px solid #FF6600;"></div>


    <div id="direcciones" style="width: 710px"></div> 

    </div>
    <div id="shade"></div>


  </div>


  <div id="footer">
     
  </div>






</body>
</html>