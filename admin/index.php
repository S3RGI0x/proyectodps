<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="author" content="S3RGI0" />
<title>ProyectoDPS |</title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/navi.css" media="screen" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script src="./../js/jquery-latest.js" type="text/javascript"></script>
<script src="./../js/jquery.validate.js" type="text/javascript"></script>
<script src="./../js/funciones.js" type="text/javascript"></script>
<script src="./../js/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(function(){
	$(".box .h_title").not(this).next("ul").hide("normal");
	$(".box .h_title").not(this).next("#home").show("normal");
	$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
});
</script>
</head>
<body>
<div class="wrap">
	<!-- Menu -->
	<?php include('includes/menu.php'); ?>
	
	
	<div id="content">
		<!-- Modulos -->
		<?php include('includes/modulos.php'); ?>
		
		<div id="main">

			<?php
			$pagina = '';
			if(!isset($_GET['pagina'])){
				$pagina = 'Inicio';
			}else{
				$pagina=$_GET['pagina'];
			}

			switch ($pagina) {
				case 'Inicio':
				include("includes/principal.php");
				break;
				case 'Gestion_Usuarios':
				include("includes/gestionUsuario.php");
				break;
				case 'Opciones_Usuarios':
				include("includes/opcionesUsuario.php");
				break;
				case 'Solicitudes_Pendientes':
				include("includes/solicitudesPendientes.php");
				break;
				case 'Buscar_Solicitudes':
				include("includes/buscarSolicitudes.php");
				break;
				case 'Ver_Mensajes':
				include("includes/mensajes.php");
				break;
				case 'Visualizar_Solicitud':
				include("includes/versolicitud.php");
				break;
				default:
				include("includes/principal.php");
			} 
			?> 


		</div>
		<div class="clear"></div>
	</div>

	<<div id="footer">
		<div class="left">
			<p>ProyectoDPS <!--<a href="http://kilab.pl">Paweł Balicki</a> | Admin Panel: <a href="">YourSite.com--></a></p>
		</div>
		<div class="right">
			<!--<p><a href="">Example link 1</a> | <a href="">Example link 2</a></p>-->
		</div>
	</div> 
</div>

</body>
</html>