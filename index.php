<?php
//session_start();
require_once("requieres/funciones.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>ProyectoDPS | <?php if (!isset($_GET['pagina'])){echo 'Inicio';}else{ echo $_GET['pagina']; } ?></title>


  <!-- Librerias y Estilos (Principal) -->
  <link href="css/principal.css" rel="stylesheet" type="text/css" />
  <link href="css/styles.css" rel="stylesheet" type="text/css" />
  <!-- Menu Top -->
  <script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
  <script src="js/slide.js" type="text/javascript"></script>
  <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />	
  <!-- Validaciones -->
  <script src="js/jquery-latest.js" type="text/javascript"></script>
  <script src="js/jquery.validate.js" type="text/javascript"></script>
  <script src="js/funciones.js" type="text/javascript"></script>
  <script src="js/jquery.min.js" type="text/javascript"></script>
  
  <!-- API Google Maps 
  <script src="http://maps.google.com/maps?file=api&amp;v=2&oe=ISO-8859-1;&amp;key=ABQIAAAAXfyLOV-DBHsmkpuY-LUUzBRvMuQNe3bCQ9tCDXjXwHZUjgdNBhQG32AJg5mKqo03Qmq9WX7GTGdmvw" type="text/javascript"></script> 
  -->

</head>
<body>
  <!-- Menu Login Registro -->
  <?php include('includes/menuTop.php'); ?>

  <!-- imagen de fondo -->
  <img src="imagenes/globe.png" id="fondo" alt="">

  <!-- Menu Principal -->
  <?php include('includes/menu.php'); ?>

  <!-- contenido principal -->
  <div id="contenido">


    <?php
    $pagina = '';
    if(!isset($_GET['pagina'])){
     $pagina = 'Inicio';
    }else{
      $pagina=$_GET['pagina'];
    }
    
      switch ($pagina) {
       case 'Inicio':
       include("includes/paginas/principal.php");
       break;
       case 'Acerca_de':
       include("includes/paginas/acercade.php");
       break;
       case 'Mision':
       include("includes/paginas/mision.php");
       break;
       case 'Vision':
       include("includes/paginas/vision.php");
       break;
       case 'Valores':
       include("includes/paginas/valores.php");
       break;
       case 'Servicios':
       include("includes/paginas/servicios.php");
       break;
       case 'Contacto':
       include("includes/paginas/contacto.php");
       break;
       case 'Recuperar_Contrasenia':
       include("includes/paginas/recobrarpass.php");
       break;
       case 'Salir':
       include("includes/paginas/salir.php");
       break;
       case 'Solicitud':
       include("includes/paginas/solicitud.php");
       break;
       default:
       include("includes/paginas/principal.php");
       } 
    ?> 
    

      <div id="footer"><?php include("includes/footer.php"); ?></div>

  </div>








</body>
</html>