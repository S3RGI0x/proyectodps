
//Validacion de Formulario Login y Registro
var ar = jQuery.noConflict();
var ab = jQuery.noConflict();

ar(document).ready(function(){

    // Validacion y Login de Usuario
	ar("#FormularioLogin").validate({
		submitHandler: function(form) {
			ar.post( ar("#FormularioLogin").attr("action"),
	        ar("#FormularioLogin :input").serializeArray(),
			function(data) {
				if(data == '1'){
					location.reload();
					
				}else{
                   ar("div#resp").html(data);
				}
			  
			});
		}
	});



    // Validacion Registro de Usuario

    $('form#FormularioRegistro').each(function () {
    	ar("form#FormularioRegistro").validate({
    		submitHandler: function(form) {
    			ar.post( ar("form#FormularioRegistro").attr("action"),
    				ar("form#FormularioRegistro :input").serializeArray(),
    				function(data) {
    					if(data == '1'){
    						location.reload();
    						
    					}else{
    						ar("div#resp2").html(data);
    					}
    					
    				});
    		}
    		
    	});

    });


	// Inicializar Funciones Google Maps
	window.onload=function(){initialize();};
	window.onunload=function(){GUnload();};

});


	function cargarRutas() {
       ar("#mostrarDatos").fadeOut("slow");
       setTimeout ("obtenerRutas();", 5000); 
	}
	
	function obtenerRutas() {
		var rutas = document.getElementsByTagName('td');
        var nombres = new Array();
        var j = 0; 

		for(var i=0; i<rutas.length; i++) {
			if (rutas[i].getAttribute('jscontent')=='address') {
               	nombres[j] = rutas[i].innerHTML;
               	j++;
         
			}
		}
		document.getElementById("sorigen").innerHTML = nombres[0];
		document.getElementById("sdestino").innerHTML = nombres[1];

		var km = document.getElementsByTagName('div');
		for(var i=0; i<km.length; i++) {
			if (km[i].getAttribute('jseval')=='this.innerHTML = $Route.summaryHtml') {
				document.getElementById("skm").innerHTML = km[i].innerHTML;
               	//alert(km[i].innerHTML);
               	break;
			}
		}

		//ar('#mostrarDatos').animate({"opacity": "toggle"},800);
		ar("#mostrarDatos").fadeIn("slow");


	}


