var b = jQuery.noConflict();
b(document).ready(function() {
	
	// Expand Panel
	b("#open").click(function(){
		b("div#panel").slideDown("slow");
	
	});	
	
	// Collapse Panel
	b("#close").click(function(){
		b("div#panel").slideUp("slow");	
	});		
	
	// Switch buttons from "Log In | Register" to "Close Panel" on click
	b("#toggle a").click(function () {
		b("#toggle a").toggle();
	});		
		
});